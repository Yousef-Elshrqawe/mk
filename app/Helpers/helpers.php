<?php


use App\Models\Cart;
use App\Models\CartItem;
use App\Models\Coupon;
use App\Models\SiteSetting;
use App\Traits\ResponseTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File ;
use Intervention\Image\Facades\Image;




function deleteFile($file_name, $directory = 'unknown'): void
{
    if ($file_name && $file_name != 'default.png' && file_exists("storage/images/$directory/$file_name")) {
        unlink("storage/images/$directory/$file_name");
    }
}
function uploadAllTyps($file, $directory, $width = null, $height = null)
{
    if (!File::isDirectory('storage/images/' . $directory)) {
        File::makeDirectory('storage/images/' . $directory, 0777, true, true);
    }

    $fileMimeType = $file->getClientmimeType();
    $imageCheck = explode('/', $fileMimeType);

    if ($imageCheck[0] == 'image') {
        $allowedImagesMimeTypes = ['image/jpeg', 'image/jpg', 'image/png'];
        if (!in_array($fileMimeType, $allowedImagesMimeTypes))
            return 'default.png';

        return uploadeImage($file, $directory, $width, $height);
    }

    $allowedMimeTypes = ['application/pdf', 'application/msword', 'application/excel', 'application/vnd.ms-excel', 'application/vnd.msexcel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'];
    if (!in_array($fileMimeType, $allowedMimeTypes))
        return 'default.png';
    return uploadFile($file, $directory);
}
function uploadFile($file, $directory)
{
    $filename = time() . rand(1000000, 9999999) . '.' . $file->getClientOriginalExtension();
    $path = base_path() . '/public/storage/images/' . $directory;
    $file->move($path, $filename);
    return $filename;
}
function uploadeImage($file, $directory, $width = null, $height = null )
{
    $img = Image::make($file)->orientate();
    $thumbsPath = 'storage/images/' . $directory;
    $name = time() . '_' .   rand(1111, 9999) . '.' . $file->getClientOriginalExtension();

    if (null != $width && null != $height)
        $img->resize($width, $height, function ($constraint) {
            $constraint->aspectRatio();
        });

    $img->save($thumbsPath . '/' . $name);
    return (string)$name;
}

function getImage($name, $directory) {
    return asset("storage/images/$directory/" . $name);
}
function defaultImage($directory) {
    return asset("/storage/images/$directory/default.png");
}







