<?php

/*
|--------------------------------------------------------------------------
| Authentication Language Lines
|--------------------------------------------------------------------------
|
| The following language lines are used during authentication for various
| messages that we need to display to the user. You are free to modify
| these language lines according to your application's requirements.
|
*/

return [
'Welcome to' => 'اهلا بك في',
    'About Us' => 'من نحن ؟',
    'read more' => 'اقرءة المزيد',
    'Top Products', 'المنتجات الشائعة',
    'All Products', 'جميع المنتجات',
    'Our Partners', 'شركائنا',
    'View more', 'عرض المزيد',
    'Our achievements', 'انجازاتنا',
    'experience' => 'خبرة',
    'Employees' => 'الموظفين',
    'Partners' => 'الشركاء',
    'products' => 'المنتجات',
    'WHO WE ARE' => 'من نحن',
    'Our Vision' => 'رؤيتنا',
    'Our Mission' => 'مهمتنا',
    'categories' => 'الاقسام',
    'Showing' => 'عرض',
    'of' => 'من',
    'Contact us' => 'اتصل بنا',
    'Email' => 'البريد الالكتروني',
    'Number' => 'رقم الهاتف',
    'Location' => 'العنوان',
    'full name' => 'الاسم بالكامل',
    'email' => 'البريد الالكتروني',
    'message' => 'الرسالة',
    'Send' => 'ارسال',
    'Home' => 'الرئيسية',
    'about us' => 'من نحن',
    'Products' => 'المنتجات',
    'Our Partners' => 'شركائنا',
    'contact us' => 'اتصل بنا',
    'English' => 'English',
    'Arabic' => 'العربية',
    'Top Products' => 'المنتجات الشائعة',
    'Our_Achievements' => 'انجازاتنا',
    'HELPFUL LINKS' => 'روابط مفيدة',

];
