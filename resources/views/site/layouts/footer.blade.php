


  <!-- start footer -->
  <footer>
      <div class="box footer">
          <div class="footerLogo">
              <img src="{{asset('site/media/logo.png')}}" alt="">
          </div>
          <div class="footerLinkGroup">
              <div class="linkGroup">
                  <h2>@lang('site.HELPFUL LINKS')</h2>
                  <ul>
                      <li><a href="{{route('home')}}"> @lang('site.Home') </a></li>
                      <li><a href="{{route('about-us')}}"> @lang('site.about us') </a></li>
                      <li><a href="{{route('cat.products')}}"> @lang('site.Products')</a></li>
                      <li><a href="{{route('partners')}}"> @lang('site.Our Partners')</a></li>
                      <li><a href="{{route('contact-us')}}"> @lang('site.contact us')</a></li>
                  </ul>
              </div>
              <div class="linkGroup cont">
                  <h2>@lang('site.contact us')</h2>
                  <ul>
                      <li><a href="##"><img src="{{asset('site/media/icons/location.png')}}" alt="">Country - City</a></li>
                      <li><a href="##"><img src="{{asset('site/media/icons/call.png')}}" alt=""> +962 1 2345 6789 </a></li>
                      <li><a href="##"><img src="{{asset('site/media/icons/mail.png')}}" alt=""> info@exmaple.com</a></li>
                  </ul>
                  <div class="media">
                      <a href="##"><i class="fa-brands fa-facebook-f"></i></a>
                      <a href="##"><i class="fa-brands fa-instagram"></i></a>
                      <a href="##"><i class="fa-brands fa-linkedin-in"></i></a>
                      <a href="##"><i class="fa-brands fa-whatsapp"></i></a>
                  </div>
              </div>
          </div>
      </div>
  </footer>
  <!-- end footer -->
  </body>
  <script src="{{asset('site/js/main.js')}}"></script>
  <script src="{{asset('site/js/homeImgs.js')}}"></script>
  <script src="{{asset('site/js/scroll.js')}}"></script>
  <script src="{{asset('site/js/swiper-bundle.min.js')}}"></script>
  <script src="{{asset('site/js/swiper.js')}}"></script>
  <script src="{{asset('site/js/counter.js')}}"></script>
  @yield('js')

  </html>
