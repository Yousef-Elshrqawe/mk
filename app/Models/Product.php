<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Product extends Model
{

    use HasFactory;
    use HasTranslations;
    protected $fillable = ['title', 'description', 'image', 'weight', 'category_id'];
    //translatable
    public $translatable = ['title', 'description' , 'weight'];




    public function setImageAttribute($value)
    {
        if (null != $value && is_file($value)) {
            isset($this->attributes['image']) ? deleteFile($this->attributes['image'], 'products') : '';
            $this->attributes['image'] = uploadAllTyps($value, 'products');
        }
    }

    public function getImageAttribute()
    {
        if ($this->attributes['image']) {
            $image = getImage($this->attributes['image'], 'products');
        } else {
            $image = defaultImage('products');
        }
        return $image;
    }

    //relation
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

}

