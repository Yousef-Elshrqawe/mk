<?php

namespace App\Http\Controllers\Backend;
use Image ;
use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Services\SettingService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;



class BackendSettingController extends Controller
{

    public function __construct()
    {
        $this->middleware('can:settings-update',   ['only' => ['index','update']]);
    }

    public function index(){
        if(! Cache::has('settings'))
        {
            Cache::rememberForever('settings', function () {
                return SettingService::appInformations(Setting::pluck('value', 'key'));
            });
        }
        $data = Cache::get('settings');
        return view('admin.settings.index',compact('data'));
    }


    public function update(Request $request){
        Cache::forget('settings');
        foreach ( $request->all() as $key => $val ){
            if (in_array($key , ['website_logo' , 'website_wide_logo' , 'default_user' , 'login_logo' ,  'website_icon' , 'website_icon_base64'  ,'website_cover' ])) {
                $img           = Image::make($val);

                if($key == 'default_user'){
                    $thumbsPath    = 'storage/images/users/default.png';
                }else if ($key == 'no_data') {
                    $thumbsPath    = 'storage/images/no_data.png';
                }else{
                    $name          = time() . rand(1000000, 9999999) . '.' . $val->getClientOriginalExtension();
                    $thumbsPath    = 'storage/images/settings/'.$name;
                    Setting::where( 'key', $key ) -> update( [ 'value' => $name ] );
                }
                $img->save($thumbsPath);
            }else if($val){
                Setting::where( 'key', $key ) -> update( [ 'value' => $val ] );
            }
        }
        if ($request->is_production) {
            Setting::where( 'key', 'is_production' ) -> update( [ 'value' => 1 ] );
        }else{
            Setting::where( 'key', 'is_production' ) -> update( [ 'value' => 0 ] );
        }

        Cache::rememberForever('settings', function () {
            return SettingService::appInformations(Setting::pluck('value', 'key'));
        });

        toastr()->success(__('تم  التحديث  بنجاح'), __('utils/toastr.successful_process_message'));
        return back();
    }


}
