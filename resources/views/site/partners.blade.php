
@extends('site.layouts.master')

@section('content')

    <!-- Start Header -->
    <header class="header">
        <h1>@lang('site.Our Partners')</h1>
        <img src="{{asset('site/media/images/CONTACT.jpg')}}" alt="">
    </header>
    <!-- End Header -->

    <!-- Start Partners -->
    <div class="partners">
        <div class="box pas">
            @foreach($partners as $partner)
                <div class="client">
                    <img src="{{$partner->image}}" alt="">
                </div>
            @endforeach
        </div>
    </div>
    <!-- End Partners -->



@endsection
