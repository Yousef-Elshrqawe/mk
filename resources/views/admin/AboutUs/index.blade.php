@extends('layouts.admin')
@section('content')
<div class="col-12 p-3">
	<div class="col-12 col-lg-12 p-0 main-box">

		<div class="col-12 px-0">
			<div class="col-12 p-0 row">
				<div class="col-12 col-lg-4 py-3 px-3">
					<span class="fas fa-categories"></span> من نحن
				</div>
				<div class="col-12 col-lg-4 p-0">
				</div>
				<div class="col-12 col-lg-4 p-2 text-lg-end">
{{--					@can('categories-create')--}}
                    @if($rows->count() == 0)
                        <a href="{{route('admin.AboutUs.create')}}">
                            <span class="btn btn-primary"><span class="fas fa-plus"></span> إضافة جديد</span>
                        </a>
                    @endif

{{--					@endcan--}}
				</div>
			</div>
			<div class="col-12 divider" style="min-height: 2px;"></div>
		</div>

		<div class="col-12 py-2 px-2 row">
			<div class="col-12 col-lg-4 p-2">
				<form method="GET">
					<input type="text" name="q" class="form-control" placeholder="بحث ... " value="{{request()->get('q')}}">
				</form>
			</div>
		</div>
		<div class="col-12 p-3" style="overflow:auto">
			<div class="col-12 p-0" style="min-width:1100px;">


			<table class="table table-bordered  table-hover">
				<thead>
					<tr>
						<th>#</th>
						<th>من نحن</th>
						<th>رؤيتنا</th>
						<th>مهمتنا</th>
						<th>تحكم</th>
					</tr>
				</thead>
				<tbody>
					@foreach($rows as $row)
					<tr>
						<td>{{$row->id}}</td>
						<td>{!! Str::of($row->about)->words(10, ' ....') !!}</td>
						<td>{!! Str::of($row->vision)->words(10, ' ....') !!}</td>
						<td>{!! Str::of($row->mission)->words(10, ' ....') !!}</td>
						<td style="width: 180px;">
{{--							@can('categories-update')--}}
							<a href="{{route('admin.AboutUs.edit',$row)}}">
							<span class="btn  btn-outline-success btn-sm font-1 mx-1">
								<span class="fas fa-wrench "></span> تعديل
							</span>
							</a>
{{--							@endcan--}}
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
			</div>
		</div>
		<div class="col-12 p-3">
			{{$rows->appends(request()->query())->render()}}
		</div>
	</div>
</div>
@endsection
