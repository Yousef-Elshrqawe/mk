
@extends('site.layouts.master')

@section('content')

    <!-- Start Header -->
    <header class="header">
        <h1>@lang('site.categories')</h1>
        <img src="{{asset('site/media/images/slider.jpg')}}" alt="">
    </header>
    <!-- End Header -->

    <!-- Start categories -->
    <div class="cats">
        <div class="box cats-inner">
            <div class="swiper mySwiper4">
                <div class="swiper-wrapper">
                    @foreach($cats as $cat)
                        <div class="swiper-slide">
                            <a href="{{route('products',$cat->id )}}" class="catCard">
                                <img src="{{$cat->image}}" alt="">
                                <h2>{{$cat->title}}</h2>
                            </a>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <!-- End categories -->


  @endsection
