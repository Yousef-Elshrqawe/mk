<?php

namespace App\Http\Controllers\Backend;


use App\Http\Requests\Product\edit;
use App\Http\Requests\Product\Store;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BackendProductController extends Controller
{
    public function __construct()
    {
        /*        $this->middleware('can:sliders-create', ['only' => ['create','store']]);
                $this->middleware('can:sliders-read',   ['only' => ['show', 'index']]);
                $this->middleware('can:sliders-update', ['only' => ['edit','update']]);
                $this->middleware('can:sliders-delete', ['only' => ['delete']]);*/
    }

    public function index(Request $request)
    {
        $rows = Product::where(function ($q) use ($request) {
            if ($request->id != null)
                $q->where('id', $request->id);
            if ($request->q != null)
                $q->where('title', 'LIKE', '%' . $request->q . '%');
        })->orderBy('id', 'DESC')->paginate();

        return view('admin.products.index', compact('rows'));
    }

    public function create()
    {
        $categories = Category::latest()->get();
        return view('admin.products.create', compact('categories'));
    }


    public function store(Store $request)
    {
        $city = Product::create($request->validated());
        toastr()->success(__('utils/toastr.category_store_success_message'), __('utils/toastr.successful_process_message'));
        return redirect()->route('admin.products.index');
    }

    public function show(Product $product)
    {
    }


    public function edit(Product $product)
    {
        $categories = Category::latest()->get();

        return view('admin.products.edit', compact('product', 'categories'));
    }

    public function update(edit $request, Product $product)
    {
        $product->update($request->validated());
        toastr()->success(__('utils/toastr.category_update_success_message'), __('utils/toastr.successful_process_message'));
        return redirect()->route('admin.products.index');
    }


    public function destroy(Product $product)
    {
        $product->delete();
        toastr()->success(__('utils/toastr.category_destroy_success_message'), __('utils/toastr.successful_process_message'));
        return redirect()->route('admin.products.index');
    }
}
