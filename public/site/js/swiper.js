var swiper = new Swiper(".mySwiper", {
  slidesPerView: 1,
  spaceBetween: 20,
  keyboard: {
    enabled: true,
  },
  autoplay: {
    delay: 1500,
    disableOnInteraction: true,
  },
  breakpoints: {
    300: {
      slidesPerView: 1,
    },
    360: {
      slidesPerView: 2,
    },
    421: {
      slidesPerView: 2.5,
    },
    520: {
      slidesPerView: 3,
    },
    750: {
      slidesPerView: 3.5,
    },
    850: {
      slidesPerView: 4,
    },
    1000: {
      slidesPerView: 5,
    },
    1250: {
      slidesPerView: 6,
    },
  },
});

var swiper2 = new Swiper(".mySwiper2", {
  slidesPerView: 1,
  keyboard: {
    enabled: true,
  },
  autoplay: {
    delay: 1500,
    disableOnInteraction: true,
  },
  pagination: {
    el: ".swiper-pagination",
  },
});

var swiper3 = new Swiper(".mySwiper3", {
  slidesPerView: 1,
  spaceBetween: 30,
  keyboard: {
    enabled: true,
  },
  autoplay: {
    delay: 1500,
    disableOnInteraction: true,
  },
  breakpoints: {
    300: {
      slidesPerView: 1,
    },
    360: {
      slidesPerView: 1.5,
    },
    480: {
      slidesPerView: 2,
    },
    600: {
      slidesPerView: 2.5,
    },
    800: {
      slidesPerView: 3.5,
    },
    900: {
      slidesPerView: 4,
    },
    1100: {
      slidesPerView: 5,
    },
    1250: {
      slidesPerView: 5,
    },
  },
});
var swiper3 = new Swiper(".mySwiper4", {
  slidesPerView: 1,
  spaceBetween: 30,
  keyboard: {
    enabled: true,
  },
  autoplay: {
    delay: 1500,
    disableOnInteraction: true,
  },
  breakpoints: {
    300: {
      slidesPerView: 1,
    },
    360: {
      slidesPerView: 1.5,
    },
    480: {
      slidesPerView: 2,
    },
    600: {
      slidesPerView: 2.5,
    },
    800: {
      slidesPerView: 3.5,
    },
    900: {
      slidesPerView: 4,
    },
    1100: {
      slidesPerView: 4.5,
    },
    1250: {
      slidesPerView: 5,
    },
  },
});