<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Slider\edit;
use App\Http\Requests\Slider\Store;
use App\Models\Sliders;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class BackendSlidersController extends Controller
{
    public function __construct()
    {
/*        $this->middleware('can:sliders-create', ['only' => ['create','store']]);
        $this->middleware('can:sliders-read',   ['only' => ['show', 'index']]);
        $this->middleware('can:sliders-update', ['only' => ['edit','update']]);
        $this->middleware('can:sliders-delete', ['only' => ['delete']]);*/
    }

    public function index(Request $request)
    {
        $rows =  Sliders::where(function($q)use($request){
            if($request->id!=null)
                $q->where('id',$request->id);
            if($request->q!=null)
                $q->where('title','LIKE','%'.$request->q.'%');
        })->orderBy('id','DESC')->paginate();

        return view('admin.Sliders.index',compact('rows'));
    }

    public function create()
    {
        return view('admin.Sliders.create');
    }


    public function store(Store $request)
    {
        if ($request->type == 4 || $request->type == 5) {
            Sliders::where('type', $request->type)->delete();
        }
        $city = Sliders::create($request->validated());
        toastr()->success(__('utils/toastr.category_store_success_message'), __('utils/toastr.successful_process_message'));
        return redirect()->route('admin.sliders.index');
    }

    public function show(Category $category)
    {
    }


    public function edit(Sliders $slider)
    {
        return view('admin.Sliders.edit',compact('slider'));
    }

    public function update(edit $request, Sliders $slider)
    {
        if ($request->type == 4 || $request->type == 5) {
            Sliders::where('type', $request->type)->delete();
        }
        $slider->update($request->validated());
        toastr()->success(__('utils/toastr.category_update_success_message'), __('utils/toastr.successful_process_message'));
        return redirect()->route('admin.sliders.index');
    }


    public function destroy(Sliders $slider)
    {
        if ($slider->type == 4 || $slider->type == 5) {
            toastr()->error(__('utils/toastr.category_destroy_error_message'), __('utils/toastr.error_message'));
            return redirect()->route('admin.sliders.index');
        }
        $slider->delete();
        toastr()->success(__('utils/toastr.category_destroy_success_message'), __('utils/toastr.successful_process_message'));
        return redirect()->route('admin.sliders.index');
    }
}
