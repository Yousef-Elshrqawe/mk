<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sliders extends Model
{
    use HasFactory;

    protected $guarded = [
    ];

    public function setImageAttribute($value)
    {
        if (null != $value && is_file($value)) {
            isset($this->attributes['image']) ? deleteFile($this->attributes['image'], 'sliders') : '';
            $this->attributes['image'] = uploadAllTyps($value, 'sliders');
        }
    }

    public function getImageAttribute()
    {
        if ($this->attributes['image']) {
            $image = getImage($this->attributes['image'], 'sliders');
        } else {
            $image = defaultImage('sliders');
        }
        return $image;
    }

    //boot delete image
    protected static function boot()
    {
        parent::boot();
        static::deleting(function ($slider) {
            deleteFile($slider->image, 'sliders');
        });
    }
}
