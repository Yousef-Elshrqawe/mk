<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SettingResource extends JsonResource
{
    private $data               = [];
    public function setToken($value) {
        $this->data = $value;
        return $this;
    }

    public function toArray($request)
    {
        return [
            'site-name' => $this->data['website_name'],
            'website_logo' => $this->data['website_logo'],
            'website_cover' => $this->data['website_cover'],
            'main_color' => $this->data['main_color'],
            'whatsapp_phone' => $this->data['whatsapp_phone'],
            'facebook_link' => $this->data['facebook_link'],
            'twitter_link' => $this->data['twitter_link'],
            'instagram_link' => $this->data['instagram_link'],
        ];
    }
}
