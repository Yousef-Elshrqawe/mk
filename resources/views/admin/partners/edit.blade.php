@extends('layouts.admin')
@section('content')
    <div class="col-12 p-3">
        <div class="col-12 col-lg-12 p-0 ">


            <form id="validate-form" class="row" enctype="multipart/form-data" method="POST"
                  action="{{route('admin.partners.update',$partner)}}">
                @csrf
                @method("PUT")
                <input type="hidden" name="temp_file_selector" id="temp_file_selector" value="{{uniqid()}}">
                <div class="col-12 col-lg-8 p-0 main-box">
                    <div class="col-12 px-0">
                        <div class="col-12 px-3 py-3">
                            <span class="fas fa-info-circle"></span> تعديل
                        </div>
                        <div class="col-12 divider" style="min-height: 2px;"></div>
                    </div>
                    <div class="col-12 p-3 row">


                        <div class="col-12 col-lg-12 p-2">
                            <div class="col-12">
                                الاسم العربي
                            </div>
                            <div class="col-12 pt-3">
                                <input type="text" name="name[ar]" required maxlength="190" class="form-control"
                                       value="{{$partner->getTranslation('name','ar')}}">
                            </div>
                        </div>

                        <div class="col-12 col-lg-12 p-2">
                            <div class="col-12">
                                الاسم الانجليزي
                            </div>
                            <div class="col-12 pt-3">
                                <input type="text" name="name[en]" required maxlength="190" class="form-control"
                                       value="{{$partner->getTranslation('name','en')}}">
                            </div>
                        </div>

                        <div class="col-12 col-lg-12 p-2">
                            <div class="col-12">
                                وصف بالعربي
                            </div>
                            <div class="col-12 pt-3">
                                <textarea type="text" name="description[ar]" required maxlength="190" class="form-control editor with-file-explorer"
                                > {!! $partner->getTranslation('description','ar') !!}</textarea>
                            </div>
                        </div>

                        <div class="col-12 col-lg-12 p-2">
                            <div class="col-12">
                                وصف بالانجليزي
                            </div>
                            <div class="col-12 pt-3">
                                <textarea type="text" name="description[en]" required maxlength="190" class="form-control editor with-file-explorer"
                                > {!! $partner->getTranslation('description','en') !!}</textarea>
                            </div>
                        </div>








                        <div class="col-12 p-2">
                            <div class="col-12">
                                الشعار
                            </div>
                            <div class="col-12 pt-3">
                                <input type="file" name="image" class="form-control" accept="image/*">
                            </div>
                            <div class="col-12 pt-3">
                                <img src="{{$partner->image}}" style="width:100px">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 p-3">
                    <button class="btn btn-success" id="submitEvaluation">حفظ</button>
                </div>
            </form>
        </div>
    </div>
@endsection
