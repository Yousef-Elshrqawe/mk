<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Partner\edit;
use App\Http\Requests\Partner\Store;
use App\Models\Partner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class BackendPartnerController extends Controller
{
    public function __construct()
    {
        /*        $this->middleware('can:sliders-create', ['only' => ['create','store']]);
                $this->middleware('can:sliders-read',   ['only' => ['show', 'index']]);
                $this->middleware('can:sliders-update', ['only' => ['edit','update']]);
                $this->middleware('can:sliders-delete', ['only' => ['delete']]);*/
    }

    public function index(Request $request)
    {
        $rows =  Partner::where(function($q)use($request){
            if($request->id!=null)
                $q->where('id',$request->id);
            if($request->q!=null)
                $q->where('title','LIKE','%'.$request->q.'%');
        })->orderBy('id','DESC')->paginate();

        return view('admin.partners.index',compact('rows'));
    }

    public function create()
    {
        return view('admin.partners.create');
    }


    public function store(Store $request)
    {
        $city = Partner::create($request->validated());
        toastr()->success(__('utils/toastr.category_store_success_message'), __('utils/toastr.successful_process_message'));
        return redirect()->route('admin.partners.index');
    }

    public function show(Partner $partner)
    {
    }


    public function edit(Partner $partner)
    {
        return view('admin.partners.edit',compact('partner'));
    }

    public function update(edit $request, Partner $partner)
    {
        $partner->update($request->validated());
        toastr()->success(__('utils/toastr.category_update_success_message'), __('utils/toastr.successful_process_message'));
        return redirect()->route('admin.partners.index');
    }


    public function destroy(Partner $partner)
    {
        $partner->delete();
        toastr()->success(__('utils/toastr.category_destroy_success_message'), __('utils/toastr.successful_process_message'));
        return redirect()->route('admin.partners.index');
    }
}
//إنجازاتنا
