let m1 = document.querySelector('.m1');
let m2 = document.querySelector('.m2');
let imgs = document.querySelector('.imgs');

m1.addEventListener('mouseover',()=>{
  m1.classList.add('active');
  imgs.classList.add('active');
})

m1.addEventListener('mouseleave',()=>{
  m1.classList.remove('active');
  imgs.classList.remove('active');
})

m2.addEventListener('mouseover',()=>{
  m2.classList.add('active2');
  imgs.classList.add('active2');
})

m2.addEventListener('mouseleave',()=>{
  m2.classList.remove('active2');
  imgs.classList.remove('active2');
})

let play = document.getElementById('play');
let home = document.querySelector('.home');

play.addEventListener('click',()=>{
  home.classList.toggle('pause');
})