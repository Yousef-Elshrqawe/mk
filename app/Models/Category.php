<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\HasMedia;
use Spatie\Image\Manipulations;

class Category extends BaseModel implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;

    const IMAGEPATH = 'categores';




    public $guarded=['id','created_at','updated_at'];



    public function getUrlAttribute(){
        return route('category.show',$this);
    }
    public function Hotels()
    {
        return $this->hasMany(Hotels::class);
    }

}
