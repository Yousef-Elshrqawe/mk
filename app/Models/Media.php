<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
#use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Spatie\MediaLibrary\MediaCollections\Models\Media as MediaModel;

class Media extends BaseModel
{
    use HasFactory;
    const IMAGEPATH = 'products';
    protected $guarded =[];
    public function media() : MorphTo
    {
        return $this->morphTo();
    }
}
