<?php
namespace  App\Services;

class SettingService {

    public static function appInformations($app_info)
    {
        $data                        = [
            //http://127.0.0.1:8001/storage/images/settings/16795907993527938.png
            'website_name'                   =>$app_info['website_name'],
            'website_bio'                    =>$app_info['website_bio'],
            'website_logo'                   =>(url('/storage/images/settings/').'/'. $app_info['website_logo']),
            'website_wide_logo'              =>(url('/storage/images/settings/').'/'. $app_info['website_wide_logo']),
            'website_icon'                   =>(url('/storage/images/settings/').'/'. $app_info['website_icon']),
            'website_icon_base64'            =>(url('/storage/images/settings/').'/'. $app_info['website_icon_base64']),
            'website_cover'                  =>(url('/storage/images/settings/').'/'. $app_info['website_cover']),

//            'default_user'                   => (url('public/storage/images/users/'). '/'.$app_info['default_user']),

            'address'                        =>$app_info['address'],
            'main_color'                     =>$app_info['main_color'],
            'hover_color'                    =>$app_info['hover_color'],
            'dashboard_dark_mode'            =>$app_info['dashboard_dark_mode'],
            'contact_email'                  =>$app_info['contact_email'],
            'phone'                          => $app_info['phone'],
            'phone2'                         => $app_info['phone2'],
            'whatsapp_phone'                 => $app_info['whatsapp_phone'],
            'facebook_link'                  => $app_info['facebook_link'],
            'telegram_link'                  => $app_info['telegram_link'],
            'whatsapp_link'                  =>$app_info['whatsapp_link' ],
            'twitter_link'                   =>$app_info['twitter_link'],
            'instagram_link'                 =>$app_info['instagram_link'],
            'youtube_link'                   =>$app_info['youtube_link' ],
            'tiktok_link'                    =>$app_info['tiktok_link'],
            'upwork_link'                    =>$app_info['upwork_link'],
            'nafezly_link'                   =>$app_info['nafezly_link'],
            'linkedin_link'                  =>$app_info['linkedin_link'],
            'github_link'                    =>$app_info['github_link'],
            'another_link1'                  =>$app_info['another_link1'],
            'another_link2'                  =>$app_info['another_link2'],
            'another_link3'                  =>$app_info['another_link3'],
            'contact_page'                   =>$app_info['contact_page' ],
            'header_code'                    =>$app_info['header_code'],
            'footer_code'                    =>$app_info['footer_code'],
            'robots_txt'                     =>$app_info['robots_txt' ],
        ];
        return $data;
    }



}
