@extends('layouts.admin')
@section('content')
    <div class="col-12 p-3">
        <div class="col-12 col-lg-12 p-0 ">


            <form id="validate-form" class="row" enctype="multipart/form-data" method="POST"
                  action="{{route('admin.sliders.update',$slider)}}">
                @csrf
                @method("PUT")
                <input type="hidden" name="temp_file_selector" id="temp_file_selector" value="{{uniqid()}}">
                <div class="col-12 col-lg-8 p-0 main-box">
                    <div class="col-12 px-0">
                        <div class="col-12 px-3 py-3">
                            <span class="fas fa-info-circle"></span> تعديل
                        </div>
                        <div class="col-12 divider" style="min-height: 2px;"></div>
                    </div>
                    <div class="col-12 p-3 row">


                        <div class="col-12 col-lg-6 p-2">
                            <div class="col-12">
                                العنوان
                            </div>
                            <div class="col-12 pt-12">
                                <input type="text" name="title" required maxlength="190" class="form-control"
                                       value="{{$slider->title}}">
                            </div>
                        </div>

                        <div class="col-12 col-lg-12 p-2">
                            <div class="col-12">
                                القسم
                            </div>
                            <div class="col-12 pt-3">
                                <select name="type" required class="form-control">
                                    <option value="1" @if($slider->type == 1) selected @endif>الرئيسيه</option>
                                    <option value="4" @if($slider->type == 4) selected @endif>اسلايدر الرئيسيه من جه اليمين</option>
                                    <option value="5" @if($slider->type == 5) selected @endif>اسلايدر الرئيسيه من جه اليسار</option>
                                    <option value="2" @if($slider->type == 2) selected @endif>الفرعيه</option>
                                    <option value="3" @if($slider->type == 3) selected @endif>من نحن</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-12 col-lg-12 p-2">
                            <div class="col-12">
                                الحاله
                            </div>
                            <div class="col-12 pt-3">
                                <select name="status" required class="form-control">
                                    <option value="1" @if($slider->status == 1) selected @endif>مفعل</option>
                                    <option value="2" @if($slider->status == 2) selected @endif>غير مفعل</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-12 p-2">
                            <div class="col-12">
                                الشعار
                            </div>
                            <div class="col-12 pt-3">
                                <input type="file" name="image" class="form-control" accept="image/*">
                            </div>
                            <div class="col-12 pt-3">
                                <img src="{{$slider->image}}" style="width:100px">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 p-3">
                    <button class="btn btn-success" id="submitEvaluation">حفظ</button>
                </div>
            </form>
        </div>
    </div>
@endsection
