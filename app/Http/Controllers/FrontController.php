<?php

namespace App\Http\Controllers;

use App\Models\AboutUs;
use App\Models\achievement;
use App\Models\Partner;
use App\Models\Product;
use App\Models\Sliders;
use Illuminate\Http\Request;
use App\Models\Article;
use App\Models\Tag;
use App\Models\Contact;
use App\Models\ArticleComment;
use App\Models\Page;
use App\Models\Category;
use Illuminate\Support\Facades\Session;


class FrontController extends Controller
{

    //lang
    public function lang($lang)
    {
        Session::put('lang',$lang);
        return redirect()->back();
    }
    public function index(Request $request)
    {
        $sliders = Sliders::all();
        $about = AboutUs::first();
        $products = Product::orderBy('id','DESC')->limit(6)->get();
        $partners = Partner::orderBy('id','DESC')->limit(6)->get();
        $achievement = achievement::first();
        return view('site.index',compact('sliders','about','products','partners','achievement'));
    }

    //about us
    public function about_us(Request $request)
    {
        $about = AboutUs::first();
        $sliders = Sliders::all();
        $partners = Partner::orderBy('id','DESC')->limit(6)->get();
        $achievement = achievement::first();
        return view('site.about',compact('about','sliders','partners','achievement'));
    }

    //contact us
    public function contact_us(Request $request)
    {
        return view('site.contact');
    }

    //contact us post
    public function contact_us_post(Request $request)
    {
        $request->validate([
            'name'=>"required|min:3|max:255",
            'email'=>"required|email",
            "message"=>"required|min:10|max:10000",
        ]);
        Contact::create([
            'name'=>$request->name,
            'email'=>$request->email,
            'message'=>$request->message
        ]);

        toastr()->success('تم استلام رسالتك بنجاح وسنتواصل معك في أقرب وقت');
        //\Session::flash('message', __("Your Message Has Been Send Successfully And We Will Contact You Soon !"));
        return redirect()->back();
    }
    //catProducts
    public function catProducts(Request $request)
    {
        $cats = Category::all();
        return view('site.cat-products',compact('cats'));
    }

    //partners
    public function partners(Request $request)
    {
        $partners = Partner::all();
        return view('site.partners',compact('partners'));
    }

    //products
    public function products(Request $request , $id = null)
    {
        if($id == null){
            $rows = Product::orderBy('id','DESC')->paginate(10);
            $cats = ['title'=>'All Products'] ;
            return view('site.products',compact('rows','cats'));
        }
        $rows = Product::where('category_id',$id)->orderBy('id','DESC')->paginate(10);
        $cats = Category::find($id);
        return view('site.products',compact('rows','cats'));
    }



































    public function comment_post(Request $request)
    {
        if(auth()->check()){
            $request->validate([
                "content"=>"required|min:3|max:10000",
            ]);
            ArticleComment::create([
                'user_id'=>auth()->user()->id,
                'article_id'=>$request->article_id,
//                'content'=>$request->content,
            ]);
        }else{
            $request->validate([
                'adder_name'=>"nullable|min:3|max:190",
                'adder_email'=>"nullable|email",
                "content"=>"required|min:3|max:10000",
            ]);
            ArticleComment::create([
                'article_id'=>$request->article_id,
                'adder_name'=>$request->adder_name,
                'adder_email'=>$request->adder_email,
//                'content'=>$request->content,
            ]);
        }
        toastr()->success("تم اضافة تعليقك بنجاح وسيظهر للعامة بعد المراجعة");
        return redirect()->back();
    }

    public function contact_post(Request $request)
    {
        $request->validate([
            'name'=>"required|min:3|max:190",
            'email'=>"nullable|email",
            "phone"=>"required|numeric",
            "message"=>"required|min:3|max:10000",
        ]);
        if(\MainHelper::recaptcha($request->recaptcha)<0.8)abort(401);
        Contact::create([
            'user_id'=>auth()->check()?auth()->id():NULL,
            'name'=>$request->name,
            'email'=>$request->email,
            'phone'=>$request->phone,
            'message'=>/*"قادم من : ".urldecode(url()->previous())."\n\nالرسالة : ".*/$request->message
        ]);

        toastr()->success('تم استلام رسالتك بنجاح وسنتواصل معك في أقرب وقت');
        //\Session::flash('message', __("Your Message Has Been Send Successfully And We Will Contact You Soon !"));
        return redirect()->back();
    }
    public function category(Request $request,Category $category){
        $articles = Article::where(function($q)use($request,$category){
            if($request->user_id!=null)
                $q->where('user_id',$request->user_id);

            $q->whereHas('categories',function($q)use($request,$category){
                $q->where('category_id',$category->id);
            });
        })->with(['categories','tags'])->withCount(['comments'=>function($q){$q->where('reviewed',1);}])->orderBy('id','DESC')->paginate();
        return view('front.pages.blog',compact('articles','category'));
    }
    public function tag(Request $request,Tag $tag){

        $articles = Article::where(function($q)use($request,$tag){
            if($request->user_id!=null)
                $q->where('user_id',$request->user_id);

            $q->whereHas('tags',function($q)use($request,$tag){
                $q->where('tag_id',$tag->id);
            });
        })->with(['categories','tags'])->withCount(['comments'=>function($q){$q->where('reviewed',1);}])->orderBy('id','DESC')->paginate();

        return view('front.pages.blog',compact('articles','tag'));
    }
    public function article(Request $request,Article $article)
    {
        $article->load(['categories','comments'=>function($q){$q->where('reviewed',1);},'tags'])->loadCount(['comments'=>function($q){$q->where('reviewed',1);}]);
        $this->views_increase_article($article);
        return view('front.pages.article',compact('article'));
    }
    public function page(Request $request,Page $page)
    {
        return view('front.pages.page',compact('page'));
    }
    public function blog(Request $request)
    {
        $articles = Article::where(function($q)use($request){
            if($request->category_id!=null)
                $q->where('category_id',$request->category_id);
            if($request->user_id!=null)
                $q->where('user_id',$request->user_id);
        })->with(['categories','tags'])->withCount(['comments'=>function($q){$q->where('reviewed',1);}])->orderBy('id','DESC')->paginate();
        return view('front.pages.blog',compact('articles'));
    }
    public function views_increase_article(Article $article)
    {
        $counter = $article->item_seens()->where('type',"ARTICLE")->where('ip',\UserSystemInfoHelper::get_ip())->whereDate('created_at', \Carbon::today())->count();
        if (!$counter) {
            \App\Models\ItemSeen::create([
                'type_id'=>$article->id,
                'type'=>"ARTICLE",
                'ip'=>\UserSystemInfoHelper::get_ip(),
                'prev_link'=>\UserSystemInfoHelper::prev_url(),
                'agent_name'=>request()->header('User-Agent'),
                'browser'=>\UserSystemInfoHelper::get_browsers(),
                'device'=>\UserSystemInfoHelper::get_device(),
                'operating_system'=>\UserSystemInfoHelper::get_os()
            ]);
            $article->update(['views' => $article->views + 1]);
        }
    }
}

