<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\Achievement\Store;
use App\Models\achievement as Model ;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class BackendAchievementController extends Controller
{
    protected $view = 'achievements';

    public function __construct()
    {
        /*        $this->middleware('can:sliders-create', ['only' => ['create','store']]);
                $this->middleware('can:sliders-read',   ['only' => ['show', 'index']]);
                $this->middleware('can:sliders-update', ['only' => ['edit','update']]);
                $this->middleware('can:sliders-delete', ['only' => ['delete']]);*/

    }

    public function index(Request $request)
    {
        $rows =  Model::where(function($q)use($request){
            if($request->id!=null)
                $q->where('id',$request->id);
            if($request->q!=null)
                $q->where('title','LIKE','%'.$request->q.'%');
        })->orderBy('id','DESC')->paginate();

        return view('admin.'.$this->view.'.index',compact('rows'));
    }

    public function create()
    {
        return view('admin.'.$this->view.'.create');
    }


    public function store(Store $request)
    {
        $city = Model::create($request->validated());
        toastr()->success(__('utils/toastr.category_store_success_message'), __('utils/toastr.successful_process_message'));
        return redirect()->route('admin.'.$this->view.'.index');
    }

    public function show(Model $achievement)
    {
    }


    public function edit(Model $achievement)
    {

        return view('admin.'.$this->view.'.edit',compact('achievement'));
    }

    public function update(Store $request, Model $achievement)
    {
        $achievement->update($request->validated());
        toastr()->success(__('utils/toastr.category_update_success_message'), __('utils/toastr.successful_process_message'));
        return redirect()->route('admin.achievements.index');
    }


    public function destroy(Model $achievement)
    {
        $achievement->delete();
        toastr()->success(__('utils/toastr.category_destroy_success_message'), __('utils/toastr.successful_process_message'));
        return redirect()->route('admin.partners.index');
    }
}
//شركاؤنا
