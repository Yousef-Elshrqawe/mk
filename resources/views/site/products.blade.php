
@extends('site.layouts.master')

@section('content')

    <!-- Start Header -->
    <header class="header">
        <h1>@lang('site.products')</h1>
        <h6>@lang('site.categories') - <span>{{$cats->title ?? 'All Products'}}</span></h6>
        <img src="{{asset('site/media/images/slider.jpg')}}" alt="">
    </header>
    <!-- End Header -->

    <!-- Start Partners -->
    <div class="partners">
        <div class="box pas prdos">
            @foreach($rows as $product)
            <div class="product">
                <div class="pImg">
                    <img src="{{$product->image}}" alt="">
                </div>
                <div class="ptext">
                    <h1>{{$product->title}} </h1>
                    <div class="price">
                        <span></span>
                        <span>{{$product->weight}}</span>
                    </div>
                </div>
                <div class="co">
                    <img src="{{$product->category->image}}" alt="">
                </div>
            </div>
            @endforeach



                <div class="paginationCont">
                    <div class="page">@lang('site.Showing') <span>{{$rows->firstItem()}} - {{$rows->lastItem()}}</span> @lang('site.of') <span>{{$rows->total()}}</span></div>
                    <div class="paginations">
                        <ul class="spans">
                            <li><a href="{{ $rows->previousPageUrl() }}" class="page-link"><img src="{{asset('site/media/icons/arrow-prev.svg')}}" alt=""></a></li>
                            @for($i=1;$i<=$rows->lastPage();$i++)
                                {{-- لو  عدد الصفحات  اكبر  من  10  ضيف  النقطتين  --}}
                                @if($rows->lastPage() > 10)
                                    @if($i == 1 || $i == $rows->lastPage() || ($i >= $rows->currentPage() - 2 && $i <= $rows->currentPage() + 2) )
                                        <li class=""><a href="{{ $rows->url($i) }}" class="page-link {{ ($rows->currentPage() == $i) ? 'active' : '' }}">{{ $i }}</a></li>
                                    @elseif($i == $rows->currentPage() - 3 || $i == $rows->currentPage() + 3)
                                        <li><span class="page-link">...</span></li>
                                    @endif
                                @else
                                    <li class=""><a href="{{ $rows->url($i) }}" class="page-link {{ ($rows->currentPage() == $i) ? 'active' : '' }}">{{ $i }}</a></li>
                                @endif
                            @endfor
                            <li><a href="{{ $rows->nextPageUrl() }}" class="page-link"><img src="{{asset('site/media/icons/arrow-next.svg')}}" alt=""></a></li>
                        </ul>
                    </div>
                </div>
        </div>
    </div>
    <!-- End Products -->



@endsection
