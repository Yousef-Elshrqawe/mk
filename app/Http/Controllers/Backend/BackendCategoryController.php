<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Category\Store;
use App\Models\Category;
use Illuminate\Http\Request;

class BackendCategoryController extends Controller
{

    public function __construct()
    {
        $this->middleware('can:categories-create', ['only' => ['create','store']]);
        $this->middleware('can:categories-read',   ['only' => ['show', 'index']]);
        $this->middleware('can:categories-update', ['only' => ['edit','update']]);
        $this->middleware('can:categories-delete', ['only' => ['delete']]);
    }

    public function index(Request $request)
    {
        $categories =  Category::where(function($q)use($request){
            if($request->id!=null)
                $q->where('id',$request->id);
            if($request->q!=null)
                $q->where('title','LIKE','%'.$request->q.'%');
        })->orderBy('id','DESC')->paginate();

        return view('admin.categories.index',compact('categories'));
    }

    public function create()
    {
        return view('admin.categories.create');
    }


    public function store(Store $request)
    {
        $city = Category::create($request->validated());
        toastr()->success(__('utils/toastr.category_store_success_message'), __('utils/toastr.successful_process_message'));
        return redirect()->route('admin.categories.index');
    }

    public function show(Category $category)
    {
    }


    public function edit(Category $category)
    {
        return view('admin.categories.edit',compact('category'));
    }

    public function update(Store $request, Category $category)
    {
        $category->update($request->validated());
        toastr()->success(__('utils/toastr.category_update_success_message'), __('utils/toastr.successful_process_message'));
        return redirect()->route('admin.categories.index');
    }


    public function destroy(Category $category)
    {
        $category->delete();
        toastr()->success(__('utils/toastr.category_destroy_success_message'), __('utils/toastr.successful_process_message'));
        return redirect()->route('admin.categories.index');
    }
}
