<?php

namespace App\Observers;

use App\Models\Hotels;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;


class ProductObserver
{

    public function created(Hotels $Hotels)
    {
        //

    }


    public function updated(Hotels $Hotels)
    {
        //
    }

    /**
     * Handle the Hotels "deleted" event.
     *
     * @param  \App\Models\Hotels  $Hotels
     * @return void
     */
    public function deleted(Hotels $hotel)
    {


    }

    public function saved(Hotels $Hotels)
    {


        if (request()->has('images') && count(request()->images)) {
            $i = 1;
            foreach (request()->images as $image) {
                $file_name = $Hotels->title . '_' . time() . '_' . $i . '.' . $image->getClientOriginalExtension();
                $file_size = $image->getSize();
                $file_type = 'image/jpeg';
                $file_path = 'hotels';
                $path = base_path() . '/public/storage/images/hotels/' . $file_name ;


                Image::make($image->getRealPath())->resize(500, null, function ($constraint) {
                    $constraint->aspectRatio();
                })->save($path, 100);

                $Hotels->media()->create([
                    'file_name' => $file_name,
                    'file_size' => $file_size,
                    'file_type' => $file_type,
                    'file_status' => true,
                    'file_path' => $file_path,
                    'file_sort' => $i,
                ]);
                $i++;
            }
        }
    }

    /**
     * Handle the Hotels "restored" event.
     *
     * @param  \App\Models\Hotels  $Hotels
     * @return void
     */
    public function restored(Hotels $Hotels)
    {
        //
    }

    /**
     * Handle the Hotels "force deleted" event.
     *
     * @param  \App\Models\Hotels  $Hotels
     * @return void
     */
    public function forceDeleted(Hotels $Hotels)
    {
        //
    }
}
