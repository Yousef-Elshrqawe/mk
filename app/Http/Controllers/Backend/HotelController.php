<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\Hotels\Store;
use App\Models\Category;
use App\Models\Hotels;
use App\Models\Media;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class HotelController extends Controller
{

    public function __construct()
    {
        $this->middleware('can:hotels-create', ['only' => ['create','store']]);
        $this->middleware('can:hotels-read',   ['only' => ['show', 'index']]);
        $this->middleware('can:hotels-update', ['only' => ['edit','update']]);
        $this->middleware('can:hotels-delete', ['only' => ['delete']]);
    }

    public function index(Request $request)
    {
        $Hotels =  Hotels::where(function($q)use($request){
            if($request->id!=null)
                $q->where('id',$request->id);
            if($request->q!=null)
                $q->where('title','LIKE','%'.$request->q.'%');
        })->orderBy('id','DESC')->paginate();

        return view('admin.Hotels.index',compact('Hotels'));
    }

    public function create()
    {
        $categories = Category::get(['id', 'title']);
        return view('admin.Hotels.create', compact('categories'));
    }

    public function store(Store $request)
    {

       Hotels::create($request->validated());
        toastr()->success(__('تم  اضافه  الفندق بنجاح'), __('utils/toastr.successful_process_message'));
        return redirect()->route('admin.hotels.index');
    }


    public function show(Hotels $hotel)
    {
    }

    public function edit(Hotels $hotel)
    {

        $categories = Category::get(['id', 'title']);
        return view('admin.Hotels.edit',compact('hotel' , 'categories'));
    }


    public function update(Store $request, Hotels $hotel)
    {

        $hotel->update($request->validated());
        toastr()->success(__('تم  تحديث الفندق  بنجاح'), __('utils/toastr.successful_process_message'));
        return redirect()->route('admin.hotels.index');
    }


    public function destroy(Hotels $hotel)
    {

        if ($hotel->media()->count()) {
            foreach ($hotel->media as $media) {
                $path = base_path() . '/public/storage/images/hotels/' . $media->file_name;
                if (File::exists($path)) {
                    File::delete($path);
                }
            }
        }

        $hotel->media()->delete();
        $hotel->delete();
        toastr()->success(__('تم حذف  الفندق بنجاح'), __('utils/toastr.successful_process_message'));
        return redirect()->route('admin.hotels.index');
    }


    public function imageDestroy(int $hotels_image_id) {

        Media::findOrFail($hotels_image_id)->delete();
        return redirect()->back();
    }
}
