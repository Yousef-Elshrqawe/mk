
@extends('site.layouts.master')

@section('content')

    <!-- Start Home -->
    <div class="home">
        <div class="homeImgs">
            <div class="shadow m1 active">
                @if($sliders->where('type', 5)->first())
                    <img src="{{$sliders->where('type', 5)->first()->image}}" alt="">
                @else
                <img src="{{asset('site/media/images/doughnuts-donuts.webp')}}" alt="">
                @endif

            </div>
            <div class="imgs">
                @foreach($sliders->where('type', 1)->all() as $slider)
                <div class="shadow"><img src="{{asset('site/media/images/doughnuts-donuts.webp')}}" alt=""></div>
                @endforeach
            </div>
            <div class="shadow m2">
                @if($sliders->where('type', 4)->first())
                <img src="{{$sliders->where('type', 4)->first()->image}}" alt="">
                @else
                <img src="{{asset('site/media/images/Black-Forest-Cake.jpg')}}" alt="">
                @endif
            </div>
        </div>
        <div class="box hxt">
            <div class="homeText">
                <h1>
                    @lang('site.Welcome to') <br>
                    <span>mk group</span>
                </h1>
                <a href="{{route('about-us')}}">@lang('site.About Us')</a>
            </div>
        </div>
        <div class="homePlay" id="play">
            <i class="fa-solid fa-play"></i>
            <i class="fa-solid fa-pause"></i>
        </div>
        <a class="readM" href="#about">@lang('site.read more') <i class="fa-solid fa-angle-down"></i></a>
        <div class="preventPlay"></div>
    </div>
    <!-- End Home -->

    <div class="container">
        <!-- Start About -->
        <div class="about p50" id="about">
            <div class="box aboutCont">
                <div class="swiper mySwiper2">
                    <div class="swiper-wrapper">
                        @foreach($sliders->where('type', 2)->all() as $slider)
                        <div class="swiper-slide">
                            <img src="{{$slider->image}}" alt="">
                        </div>
                        @endforeach
                    </div>
                    <div class="swiper-pagination"></div>
                </div>
                <div class="aboutText js-scroll fade-right">
                    <h1>@lang('site.About Us')</h1>
                    <p>{!! Str::of($about->about)->words(50, ' ....') !!}</p>
                    <a href="{{route('about-us')}}">@lang('site.read more')</a>
                </div>
            </div>
        </div>
        <!-- End About -->


        <!-- Start Products -->
        <div class="clients">
            <header>
                <h1 class="js-scroll fade-top">@lang('site.Top Products')</h1>
            </header>
            <div class="box clientsInner pw0">
                <div class="swiper mySwiper3 ps js-scroll fade-btm">
                    <div class="swiper-wrapper">
                        @foreach($products as $product)
                        <div class="swiper-slide">
                            <div class="product">
                                <div class="pImg">
                                    <img src="{{$product->image}}" alt="">
                                </div>
                                <div class="ptext">
                                    <h1>{{$product->title}} </h1>
                                    <div class="price">
                                        <span></span>
                                        <span>{{$product->weight}}</span>
                                    </div>
                                </div>
                                <div class="co">
                                    <img src="{{$product->category->image}}" alt="">
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <a href="{{route('products')}}" class="viw js-scroll fade-top">
                    @lang('site.All Products')
                    <i class="fa-solid fa-angle-right"></i>
                </a>
            </div>
        </div>
        <!-- End Products -->

        <!-- Start Clients -->
        <div class="clients">
            <header>
                <h1 class="js-scroll fade-top">@lang('site.Our Partners')</h1>
            </header>
            <div class="box clientsInner pb50">
                <div class="swiper mySwiper p50 pl20 js-scroll fade-btm">
                    <div class="swiper-wrapper">
                        @foreach($partners as $partner)
                        <div class="swiper-slide">
                            <div class="client">
                                <img src="{{$partner->image}}" alt="">
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <a href="{{route('partners')}}" class="viw js-scroll fade-top">
                    @lang('site.View more')
                    <i class="fa-solid fa-angle-right"></i>
                </a>
            </div>
        </div>
        <!-- End Clients -->


        <!-- Start Counter -->
        <div class="counterInner">
            <header>
                <h1 class="js-scroll fade-top">@lang('site.Our_Achievements')</h1>
            </header>
            <div class="box counter p50">
                <div class="counterCard js-scroll fade-btm">
                    <h1 data-num="{{$achievement->years_of_experience}}">0</h1>
                    <span>@lang('site.experience')</span>
                    <i class="fa-light fa-star"></i>
                </div>
                <div class="counterCard js-scroll fade-btm">
                    <h1 data-num="{{$achievement->employees}}">0</h1>
                    <span>@lang('site.Employees')</span>
                    <i class="fa-light fa-users"></i>
                </div>
                <div class="counterCard js-scroll fade-btm">
                    <h1 data-num="{{$achievement->partners}}">0</h1>
                    <span>@lang('site.Partners')</span>
                    <i class="fa-light fa-handshake"></i>
                </div>
                <div class="counterCard js-scroll fade-btm">
                    <h1 data-num="{{$achievement->products}}">0</h1>
                    <span>@lang('site.products')</span>
                    <i class="fa-light fa-boxes-stacked"></i>
                </div>
            </div>
        </div>
        <!-- End Counter -->
    </div>

  @endsection
