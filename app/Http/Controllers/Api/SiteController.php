<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\FaqResource;
use App\Http\Resources\HotelsOfferResource;
use App\Http\Resources\SettingResource;
use App\Models\Faq;
use App\Models\Hotels;
use App\Models\Setting;
use App\Services\SettingService;
use Illuminate\Http\Request;
use App\Models\Article;
use App\Models\Tag;
use App\Models\Contact;
use App\Models\ArticleComment;
use App\Models\Page;
use App\Models\Category;
use Illuminate\Support\Facades\Cache;


class SiteController extends Controller
{

    public function categories()
    {
       return CategoryResource::collection(Category::paginate(4));

    }
    public function hotelsOffer()
    {
        return HotelsOfferResource::collection(Hotels::OrderBy('id', 'desc')->Active()->paginate(4));

    }
    public function setting()
    {

        if(! Cache::has('settings'))
        {
            Cache::rememberForever('settings', function () {
                return SettingService::appInformations(Setting::pluck('value', 'key'));
            });
        }
        $data = Cache::get('settings');
        return SettingResource::make($data)->setToken($data);

    }

    public function faq()
    {

        return FaqResource::collection(Faq::paginate(4));


    }

}

