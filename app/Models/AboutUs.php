<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class AboutUs extends Model
{
    use HasFactory;
    use HasTranslations;

    protected $table = 'about_us';
    protected $fillable = ['about' , 'vision' , 'mission'];
    protected $translatable = ['about' , 'vision' , 'mission'];
}
