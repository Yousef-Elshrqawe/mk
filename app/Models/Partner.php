<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Translatable\HasTranslations;

class Partner extends Model
{
    use HasFactory;
    use HasTranslations;

    protected $fillable = ['name', 'image', 'description'];
    protected $translatable = ['name', 'description'];

    protected $table = 'partners';

    public function setImageAttribute($value)
    {
        if (null != $value && is_file($value)) {
            isset($this->attributes['image']) ? deleteFile($this->attributes['image'], 'partners') : '';
            $this->attributes['image'] = uploadAllTyps($value, 'partners');
        }
    }

    public function getImageAttribute()
    {
        if ($this->attributes['image']) {
            $image = getImage($this->attributes['image'], 'partners');
        } else {
            $image = defaultImage('partners');
        }
        return $image;
    }

}
