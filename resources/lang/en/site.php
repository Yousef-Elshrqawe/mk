<?php

/*
|--------------------------------------------------------------------------
| Authentication Language Lines
|--------------------------------------------------------------------------
|
| The following language lines are used during authentication for various
| messages that we need to display to the user. You are free to modify
| these language lines according to your application's requirements.
|
*/

return [
    'Welcome to' => 'Welcome to',
    'About Us' => 'About Us ?',
    'read more' => 'read more',
    'Top Products', 'Top Products',
    'All Products', 'All Products',
    'Our Partners', 'Our Partners',
    'View more', 'View more',
    'Our achievements', 'Our achievements',
    'experience' => 'experience',
    'Employees' => 'Employees',
    'Partners' => 'Partners',
    'products' => 'products',
    'WHO WE ARE' => 'WHO WE ARE',
    'Our Vision' => 'Our Vision',
    'Our Mission' => 'Our Mission',
    'categories' => 'categories',
    'Showing' => 'Showing',
    'of' => 'of',
    'Contact us' => 'Contact us',
    'Email' => 'Email',
    'Number' => 'Number',
    'Location' => 'Location',
    'full name' => 'full name',
    'email' => 'email',
    'message' => 'message',
    'Send' => 'Send',
    'Home' => 'Home',
    'about us' => 'about us',
    'Products' => 'Products',
    'Our Partners' => 'Our Partners',
    'contact us' => 'contact us',
    'English' => 'English',
    'Arabic' => 'Arabic',
    'Top Products' => 'Top Products',
    'Our_Achievements' => 'Our Achievements',
    'HELPFUL LINKS' => 'HELPFUL LINKS',



];
