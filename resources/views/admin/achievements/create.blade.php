@extends('layouts.admin')
@section('content')
    <div class="col-12 p-3">
        <div class="col-12 col-lg-12 p-0 ">


            <form id="validate-form" class="row" enctype="multipart/form-data" method="POST"
                  action="{{route('admin.achievements.store')}}">
                @csrf
                <input type="hidden" name="temp_file_selector" id="temp_file_selector" value="{{uniqid()}}">
                <div class="col-12 col-lg-8 p-0 main-box">
                    <div class="col-12 px-0">
                        <div class="col-12 px-3 py-3">
                            <span class="fas fa-info-circle"></span> إضافة جديد
                        </div>
                        <div class="col-12 divider" style="min-height: 2px;"></div>
                    </div>
                    <div class="col-12 p-3 row">

                        <div class="col-12 col-lg-12 p-2">
                            <div class="col-12">
                                سنين خبرة
                            </div>
                            <div class="col-12 pt-3">
                                <input type="number" name="years_of_experience" required maxlength="190" class="form-control"
                                       value="{{old('years_of_experience')}}">
                            </div>
                        </div>

                        <div class="col-12 col-lg-12 p-2">
                            <div class="col-12">
                                الموظفين
                            </div>
                            <div class="col-12 pt-3">
                                <input type="number" name="employees" required maxlength="190" class="form-control"
                                       value="{{old('employees')}}">
                            </div>
                        </div>

                        <div class="col-12 col-lg-12 p-2">
                            <div class="col-12">
                                الشركاء
                            </div>
                            <div class="col-12 pt-3">
                                <input type="number" name="partners" required maxlength="190" class="form-control"
                                       value="{{old('partners')}}">
                            </div>
                        </div>

                        <div class="col-12 col-lg-12 p-2">
                            <div class="col-12">
                                المنتجات
                            </div>
                            <div class="col-12 pt-3">
                                <input type="number" name="products" required maxlength="190" class="form-control"
                                       value="{{old('products')}}">
                            </div>
                        </div>


                        </div>

                        <div class="col-12 p-3">
                            <button class="btn btn-success" id="submitEvaluation">حفظ</button>
                        </div>
            </form>
        </div>
    </div>
@endsection
