<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class HotelsOfferResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'name'                           => $this->title,
            'city'                           => $this->address,
            'disc'                           => $this->description,
            'type'                           => $this->category->title,
            'img'                           => MediaResource::collection($this->media),

        ];
    }
}
