@extends('layouts.admin')
@section('content')
    <div class="col-12 p-3">
        <div class="col-12 col-lg-12 p-0 ">


            <form id="validate-form" class="row" enctype="multipart/form-data" method="POST"
                  action="{{route('admin.products.update',$product)}}">
                @csrf
                @method("PUT")
                <input type="hidden" name="temp_file_selector" id="temp_file_selector" value="{{uniqid()}}">
                <div class="col-12 col-lg-8 p-0 main-box">
                    <div class="col-12 px-0">
                        <div class="col-12 px-3 py-3">
                            <span class="fas fa-info-circle"></span> تعديل
                        </div>
                        <div class="col-12 divider" style="min-height: 2px;"></div>
                    </div>
                    <div class="col-12 p-3 row">


                        <div class="col-12 col-lg-6 p-2">
                            <div class="col-12">
                                العنوان العربي
                            </div>
                            <div class="col-12 pt-12">
                                <input type="text" name="title[ar]" required maxlength="190" class="form-control"
                                       value="{{$product->getTranslation('title','ar')}}">
                            </div>
                        </div>

                        <div class="col-12 col-lg-6 p-2">
                            <div class="col-12">
                                العنوان الانجليزي
                            </div>
                            <div class="col-12 pt-12">
                                <input type="text" name="title[en]" required maxlength="190" class="form-control"
                                       value="{{$product->getTranslation('title','en')}}">
                            </div>
                        </div>

                        <div class="col-12 col-lg-12 p-2">
                            <div class="col-12">
                                الوزن/الحجم العربي
                            </div>
                            <div class="col-12 pt-3">
                                <input type="text" name="weight[ar]" required maxlength="190" class="form-control"
                                       value="{{$product->getTranslation('weight','ar')}}">
                            </div>
                        </div>

                        <div class="col-12 col lg-12 p-2">
                            <div class="col-12">
                                الوزن/الحجم الانجليزي
                            </div>
                            <div class="col-12 pt-3">
                                <input type="text" name="weight[en]" required maxlength="190" class="form-control"
                                       value="{{$product->getTranslation('weight','en')}}">

                            </div>
                        </div>

                        <div class="col-12 col-lg-12 p-2">
                            <div class="col-12">
                                القسم
                            </div>
                            <div class="col-12 pt-3">
                                <select name="category_id" required class="form-control">
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>



                        <div class="col-12 p-2">
                            <div class="col-12">
                                الشعار
                            </div>
                            <div class="col-12 pt-3">
                                <input type="file" name="image" class="form-control" accept="image/*">
                            </div>
                            <div class="col-12 pt-3">
                                <img src="{{$product->image}}" style="width:100px">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-12 p-3">
                    <button class="btn btn-success" id="submitEvaluation">حفظ</button>
                </div>
            </form>
        </div>
    </div>
@endsection
