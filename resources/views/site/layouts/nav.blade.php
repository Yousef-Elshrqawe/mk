
<!-- Start Loader -->
<div class="loader">
    <div class="loaderLogo"><img src="{{asset('site/media/logo.png')}}" alt=""></div>
    <img src="{{asset('site/media/icons/loader.svg')}}" alt="">
</div>
<!--End Loader -->

<!-- Start Nav -->
<nav>
    <div class="box f-s">
        <a href="{{route('home')}}" class="logo">
            <img src="{{asset('site/media/logo.png')}}" alt="">
        </a>
        <div class="links">
            <div class="ls">
                <ul class="mainLinks">
                    <li><a class="link {{request()->routeIs('home') ? 'active' : ''}}"
                           href="{{route('home')}}">@lang('site.Home')</a></li>
                    <li><a class="link {{request()->routeIs('about-us') ? 'active' : ''}}"
                           href="{{route('about-us')}}">@lang('site.about us')</a></li>
                    <li>
                        <a class="link {{request()->routeIs('cat.products') || request()->routeIs('products') ? 'active' : ''}}"
                           href="{{route('cat.products')}}">@lang('site.Products')</a></li>
                    <li><a class="link {{request()->routeIs('partners') ? 'active' : ''}}"
                           href="{{route('partners')}}">@lang('site.Our Partners')</a></li>
                    <li><a class="link {{request()->routeIs('contact-us') ? 'active' : ''}}"
                           href="{{route('contact-us')}}">@lang('site.contact us')</a></li>
                </ul>
                <div class="mLinks">
                    @if(App::isLocale('en'))
                        <div class="drop">
                            <a href="##">
                                <img class="langu" src="{{asset('site/media/icons/en.png')}}" alt="">
                                @lang('site.English')
                                <img class="drop-icon" src="{{asset('site/media/icons/arrow.png')}}" alt="">
                            </a>
                            <span class="dropMenu">
                          <a href="{{route('lang','ar')}}"> <img class="langu" src="{{asset('site/media/icons/ar.png')}}"
                                                          alt=""> @lang('site.Arabic') </a>
                         </span>
                        </div>
                    @else
                        <div class="drop">
                            <a href="##">
                                <img class="langu" src="{{asset('site/media/icons/ar.png')}}" alt="">
                                العربية
                                <img class="drop-icon" src="{{asset('site/media/icons/arrow.png')}}" alt="">
                            </a>
                            <span class="dropMenu">
                <a href="{{route('lang','en')}}"> <img class="langu" src="{{asset('site/media/icons/en.png')}}" alt=""> English </a>
              </span>
                        </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="last">
            <div class="mLinks">
                @if(App::isLocale('en'))
                <div class="drop">
                    <a href="##">
                        <img class="langu" src="{{asset('site/media/icons/en.png')}}" alt="">
                        English
                        <img class="drop-icon" src="{{asset('site/media/icons/arrow.png')}}" alt="">
                    </a>
                    <span class="dropMenu">
                       <a href="{{route('lang','ar')}}"> <img class="langu" src="{{asset('site/media/icons/ar.png')}}"
                                              alt=""> Arabic </a>
                           </span>
                </div>
                @else
                    <div class="drop">
                        <a href="##">
                            <img class="langu" src="{{asset('site/media/icons/ar.png')}}" alt="">
                            العربية
                            <img class="drop-icon" src="{{asset('site/media/icons/arrow.png')}}" alt="">
                        </a>
                        <span class="dropMenu">
              <a href="{{route('lang','en')}}"> <img class="langu" src="{{asset('site/media/icons/en.png')}}" alt=""> English </a>
            </span>
                    </div>
                @endif
            </div>
            <button class="menu">
                <span></span>
                <span></span>
                <span></span>
            </button>
        </div>
    </div>
</nav>
<!-- End Nav -->


<div class="goTop">
    <span>
      <i class="fa-solid fa-angle-up"></i>
    </span>
</div>
