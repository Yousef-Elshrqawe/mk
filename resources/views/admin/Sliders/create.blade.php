@extends('layouts.admin')
@section('content')
    <div class="col-12 p-3">
        <div class="col-12 col-lg-12 p-0 ">


            <form id="validate-form" class="row" enctype="multipart/form-data" method="POST"
                  action="{{route('admin.sliders.store')}}">
                @csrf
                <input type="hidden" name="temp_file_selector" id="temp_file_selector" value="{{uniqid()}}">
                <div class="col-12 col-lg-8 p-0 main-box">
                    <div class="col-12 px-0">
                        <div class="col-12 px-3 py-3">
                            <span class="fas fa-info-circle"></span> إضافة جديد
                        </div>
                        <div class="col-12 divider" style="min-height: 2px;"></div>
                    </div>
                    <div class="col-12 p-3 row">

                        <div class="col-12 col-lg-12 p-2">
                            <div class="col-12">
                                العنوان
                            </div>
                            <div class="col-12 pt-3">
                                <input type="text" name="title" required maxlength="190" class="form-control"
                                       value="{{old('title')}}">
                            </div>
                        </div>

                        <div class="col-12 col-lg-12 p-2">
                            <div class="col-12">
                                القسم
                            </div>
                            <div class="col-12 pt-3">
                                <select name="type" required class="form-control">
                                    <option value="1">اسلايدر الرئيسيه</option>
                                    <option value="4">اسلايدر الرئيسيه من جه اليمين</option>
                                    <option value="5">اسلايدر الرئيسيه من جه اليسار</option>
                                    <option value="2">اسلايدر الرئيسيه 2</option>
                                    <option value="3">اسلايدر من نحن</option>
                                </select>
                            </div>
                        </div>

                            <div class="col-12 p-2">
                                <div class="col-12">
                                    الشعار
                                </div>
                                <div class="col-12 pt-3">
                                    <input type="file" name="image" class="form-control" accept="image/*">
                                </div>
                                <div class="col-12 pt-3">

                                </div>
                            </div>


                        </div>

                        <div class="col-12 p-3">
                            <button class="btn btn-success" id="submitEvaluation">حفظ</button>
                        </div>
            </form>
        </div>
    </div>
@endsection
