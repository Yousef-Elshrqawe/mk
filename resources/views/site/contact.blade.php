
@extends('site.layouts.master')

@section('content')

    <!-- Start Header -->
    <header class="header">
        <h1>@lang('site.Contact us')</h1>
        <img src="{{asset('site/media/images/CONTACT.jpg')}}" alt="">
    </header>
    <!-- End Header -->

    <!-- Start Contact -->
    <div class="partners">
        <div class="box contact">
            <div class="location">
                <div class="linkGroup">
                    <ul class="coLinks">
                        <li>
                            <span>@lang('site.Email') :</span>
                            <a href="##"> email@example.com</a>
                        </li>
                        <li><span>@lang('site.Number') :</span><a href="##"> +962 11 123 1333 </a></li>
                        <li>
                            <span>@lang('site.Location') :</span>
                            <a href="##">
                                Street , City , Country
                            </a>
                        </li>
                    </ul>
                </div>
                <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3492525.6748842066!2d39.7640358323452!3d31.25756843235756!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x15006f476664de99%3A0x8d285b0751264e99!2z2KfZhNij2LHYr9mG!5e0!3m2!1sar!2seg!4v1707245955290!5m2!1sar!2seg"
                    style="border:0;" allowfullscreen=""
                    referrerpolicy="no-referrer-when-downgrade"></iframe>
            </div>
            <div class="form2">
                <form action="{{route('site.contact')}}" method="post">
                    @csrf
                    <div class="input">
                        <label for="">@lang('site.full name') :</label>
                        <input type="text" name="name" spellcheck="false">

                    </div>
                    <span style="color: red">
                            @error('name')
                        {{$message}}
                        @enderror
                        </span>
                    <div class="input">
                        <label for="">@lang('site.email') :</label>
                        <input type="text" name="email" spellcheck="false">
                    </div>
                    @error('email')
                    <span style="color: red">
                        {{$message}}
                    </span>
                    @enderror
                    <div class="input ta" spellcheck="false">
                        <label for="">@lang('site.message') :</label>
                        <textarea name="message" id="" spellcheck="false"></textarea>

                    </div>
                    @error('message')
                    <span style="color: red">
                        {{$message}}
                    </span>
                    @enderror

                    <div class="sendBtn">
                        <button type="submit">@lang('site.Send')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Contact -->

  @endsection
