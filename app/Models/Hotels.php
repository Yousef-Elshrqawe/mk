<?php

namespace App\Models;

use App\Observers\ProductObserver;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\Permission\Traits\HasRoles;

class Hotels extends BaseModel implements HasMedia
{
    use HasFactory;
    use InteractsWithMedia;
    use HasRoles;



    const IMAGEPATH = 'hotels';
    protected $fillable = ['title', 'image', 'active' , 'description' , 'address'  , 'category_id' ];

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function scopeActive($query)
    {
        return $query->whereActive(true);
    }
    public function scopeOffer($query){
        return $query->whereType('offer');
    }

    public function firstMedia(): MorphOne
    {
        return $this->morphOne(Media::class, 'media')->orderBy('file_sort', 'asc');
    }

    public function media(): MorphMany
    {
        return $this->MorphMany(Media::class, 'media');
    }

    protected static function booted()
    {
        parent::boot();
        Hotels::observe(ProductObserver::class);
    }
}
