<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class edit extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'title.ar' => 'required',
            'title.en' => 'required',
            'description.ar' => 'nullable',
            'description.en' => 'nullable',
            'weight.ar' => 'required',
            'weight.en' => 'required',
            'category_id' => 'required',
            'image' => 'nullable|image',
        ];
    }
}
