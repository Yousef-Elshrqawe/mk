@extends('site.layouts.master')

@section('content')

    <!-- Start Header -->
    <header class="header">
        <h1>@lang('site.About Us')</h1>
        <img src="{{asset('site/media/images/slider.jpg')}}" alt="">
    </header>
    <!-- End Header -->
    <!-- end header -->

    <div class="container">
        <!-- Start About -->
        <div class="about p50" id="about">
            <div class="box aboutCont">
                <div class="swiper mySwiper2">
                    <div class="swiper-wrapper">
                        @foreach($sliders->where('type', 3)->all() as $slider)
                            <div class="swiper-slide">
                                <img src="{{$slider->image}}" alt="">
                            </div>
                        @endforeach
                    </div>
                    <div class="swiper-pagination"></div>
                </div>
                <div class="aboutText w100">
                    <h1>@lang('site.WHO WE ARE') ?</h1>
                    <p>{!!  $about->about !!}</p>
                </div>
                <div class="aboutText w100 js-scroll fade-right">
                    <h1>@lang('site.Our Vision') <i class="fa-regular fa-eye"></i></h1>
                    <p>
                        {!!  $about->vision !!}
                    </p>
                </div>
                <div class="aboutText w100 js-scroll fade-right">
                    <h1>@lang('site.Our Mission') <i class="fa-regular fa-crosshairs"></i></h1>
                    <p>
                        {!!  $about->mission !!}
                    </p>
                </div>
            </div>
        </div>
        <!-- End About -->

        <!-- Start Partners -->
        <div class="counterInner">
            <header>
                <h1 class="js-scroll fade-top">@lang('site.Our Partners')</h1>
            </header>
            <div class="box aboutCont p50 cms">
                @foreach($partners as $partner)
                    <div class="aboutText w100 js-scroll fade-btm">
                        <h1><img src="{{$partner->image}}" alt=""> {{$partner->name}}</h1>
                        <p>
                            {!!  $partner->description !!}
                        </p>
                    </div>
                @endforeach
            </div>

        </div>
        <!-- End Partners -->

        <!-- Start Counter -->
        <div class="counterInner">
            <header>
                <h1 class="js-scroll fade-top">@lang('site.Our_Achievements')</h1>
            </header>
            <div class="box counter p50">
                <div class="counterCard js-scroll fade-btm">
                    <h1 data-num="{{$achievement->years_of_experience}}">0</h1>

                    <span>@lang('site.experience')</span>

                    <i class="fa-light fa-star"></i>
                </div>
                <div class="counterCard js-scroll fade-btm">
                    <h1 data-num="{{$achievement->employees}}">0</h1>

                    <span>@lang('site.Employees')</span>

                    <i class="fa-light fa-users"></i>
                </div>
                <div class="counterCard js-scroll fade-btm">
                    <h1 data-num="{{$achievement->partners}}">0</h1>

                    <span>@lang('site.Partners')</span>

                    <i class="fa-light fa-handshake"></i>
                </div>
                <div class="counterCard js-scroll fade-btm">
                    <h1 data-num="{{$achievement->products}}">0</h1>

                    <span>@lang('site.products')</span>

                    <i class="fa-light fa-boxes-stacked"></i>
                </div>
            </div>
        </div>
        <!-- End Counter -->
    </div>

@endsection
