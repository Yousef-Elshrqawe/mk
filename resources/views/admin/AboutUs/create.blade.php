@extends('layouts.admin')
@section('content')
    <div class="col-12 p-3">
        <div class="col-12 col-lg-12 p-0 ">


            <form id="validate-form" class="row" enctype="multipart/form-data" method="POST"
                  action="{{route('admin.AboutUs.store')}}">
                @csrf
                <input type="hidden" name="temp_file_selector" id="temp_file_selector" value="{{uniqid()}}">
                <div class="col-12 col-lg-8 p-0 main-box">
                    <div class="col-12 px-0">
                        <div class="col-12 px-3 py-3">
                            <span class="fas fa-info-circle"></span> إضافة جديد
                        </div>
                        <div class="col-12 divider" style="min-height: 2px;"></div>
                    </div>
                    <div class="col-12 p-3 row">


                        <div class="col-12 col-lg-12 p-2">
                            <div class="col-12">
                                من نحن بالعربي
                            </div>
                            <div class="col-12 pt-3">
                                <textarea type="text" name="about[ar]" required maxlength="190"
                                          class="form-control editor with-file-explorer"
                                > {{old('about.ar')}}</textarea>
                            </div>
                        </div>

                        <div class="col-12 col-lg-12 p-2">
                            <div class="col-12">
                                من نحن بالانجليزي
                            </div>
                            <div class="col-12 pt-3">
                                <textarea type="text" name="about[en]" required maxlength="190"
                                          class="form-control editor with-file-explorer"
                                > {{old('about.en')}}</textarea>
                            </div>
                        </div>


                        <div class="col-12 col-lg-12 p-2">
                            <div class="col-12">
                                رؤيتنا بالعربي
                            </div>
                            <div class="col-12 pt-3">
                                <textarea type="text" name="vision[ar]" required maxlength="190"
                                          class="form-control editor with-file-explorer"
                                > {{old('vision.ar')}}</textarea>
                            </div>
                        </div>

                        <div class="col-12 col-lg-12 p-2">
                            <div class="col-12">
                                رؤيتنا بالانجليزي
                            </div>
                            <div class="col-12 pt-3">
                                <textarea type="text" name="vision[en]" required maxlength="190"
                                          class="form-control editor with-file-explorer"
                                > {{old('vision.en')}}</textarea>
                            </div>
                        </div>

                        <div class="col-12 col-lg-12 p-2">
                            <div class="col-12">
                                رؤيتنا بالعربي
                            </div>
                            <div class="col-12 pt-3">
                                <textarea type="text" name="mission[ar]" required maxlength="190"
                                          class="form-control editor with-file-explorer"
                                > {{old('mission.ar')}}</textarea>
                            </div>
                        </div>

                        <div class="col-12 col-lg-12 p-2">
                            <div class="col-12">
                                رؤيتنا بالانجليزي
                            </div>
                            <div class="col-12 pt-3">
                                <textarea type="text" name="mission[en]" required maxlength="190"
                                          class="form-control editor with-file-explorer"
                                > {{old('mission.en')}}</textarea>
                            </div>
                        </div>

                        <div class="col-12 p-3">
                            <button class="btn btn-success" id="submitEvaluation">حفظ</button>
                        </div>
                    </div>
            </form>
        </div>
    </div>
@endsection


