<!DOCTYPE html>
<html lang="en">
<?php
$setting = \App\Models\Setting::class;
?>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="{{ asset('site/media/tap-icon.svg') }}" type="image/x-icon"/>
    <link rel="shortcut icon" href="{{ asset('site/media/tap-icon.svg') }}"/>
    <link rel="apple-touch-icon" href="{{ asset('site/media/tap-icon.svg') }}"/>
    <meta name="color-scheme" content="light only">
    <link rel="stylesheet" href="https://site-assets.fontawesome.com/releases/v6.4.2/css/all.css">
    <link rel="stylesheet" href="{{ asset('site/js/swiper-bundle.min.css') }}">
    @if (App() -> getLocale() == 'en' )
        <link rel="stylesheet" href="{{ asset('/site/scss/style.css') }}">
    @else
        <link rel="stylesheet" href="{{ asset('/site/ar-scss/style.css') }}">
    @endif
    <title>{{$setting::where('key','website_name')->first()->value}}</title>
    @yield('css')
</head>

<body>
