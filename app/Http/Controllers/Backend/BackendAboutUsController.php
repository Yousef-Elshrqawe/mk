<?php

namespace App\Http\Controllers\Backend;

use App\Http\Requests\AboutUs\Store;
use App\Models\AboutUs;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class BackendAboutUsController extends Controller
{
    public function __construct()
    {
        /*        $this->middleware('can:sliders-create', ['only' => ['create','store']]);
                $this->middleware('can:sliders-read',   ['only' => ['show', 'index']]);
                $this->middleware('can:sliders-update', ['only' => ['edit','update']]);
                $this->middleware('can:sliders-delete', ['only' => ['delete']]);*/
    }

    public function index(Request $request)
    {
        $rows =  AboutUs::where(function($q)use($request){
            if($request->id!=null)
                $q->where('id',$request->id);
            if($request->q!=null)
                $q->where('title','LIKE','%'.$request->q.'%');
        })->orderBy('id','DESC')->paginate();

        return view('admin.AboutUs.index',compact('rows'));
    }

    public function create()
    {
        return view('admin.AboutUs.create');
    }


    public function store(Store $request)
    {
        $city = AboutUs::create($request->validated());
        toastr()->success(__('utils/toastr.category_store_success_message'), __('utils/toastr.successful_process_message'));
        return redirect()->route('admin.AboutUs.index');
    }

    public function show(AboutUs $AboutU)
    {
    }


    public function edit(AboutUs $AboutU)
    {
        return view('admin.AboutUs.edit',compact('AboutU'));
    }

    public function update(Store $request, AboutUs $AboutU)
    {
        $AboutU->update($request->validated());
        toastr()->success(__('utils/toastr.category_update_success_message'), __('utils/toastr.successful_process_message'));
        return redirect()->route('admin.AboutUs.index');
    }


    public function destroy(AboutUs $AboutU)
    {
        $AboutU->delete();
        toastr()->success(__('utils/toastr.category_destroy_success_message'), __('utils/toastr.successful_process_message'));
        return redirect()->route('admin.AboutUs.index');
    }
}
