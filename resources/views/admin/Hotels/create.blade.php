@extends('layouts.admin')

@section('content')

<div class="col-12 p-3">
	<div class="col-12 col-lg-12 p-0 ">


		<form id="validate-form" class="row"  method="POST" action="{{route('admin.hotels.store')}}" enctype="multipart/form-data">
		@csrf
		<input type="hidden" name="temp_file_selector" id="temp_file_selector" value="{{uniqid()}}">
		<div class="col-12 col-lg-8 p-0 main-box">
			<div class="col-12 px-0">
				<div class="col-12 px-3 py-3">
				 	<span class="fas fa-info-circle"></span>	إضافة جديد
				</div>
				<div class="col-12 divider" style="min-height: 2px;"></div>
			</div>
			<div class="col-12 p-3 row">

				<div class="col-12">
                 اسم  الفندق (عنوان  العرض)
				</div>
				<div class="col-12 pt-3">
					<input type="text" name="title" required   maxlength="190" class="form-control" value="{{old('title')}}" >
				</div>
			</div>

            <div class="col-12 p-3 row">

                <div class="col-12">
                    عنوان الفندق
                </div>
                <div class="col-12 pt-3">
                    <input type="text" name="address" required   maxlength="190" class="form-control" value="{{old('address')}}" >
                </div>
            </div>

            <div class="col-12 p-3 row">

                <div class="col-12">
                    نبذه عن الفندق (تفاصيل  العرض)
                </div>
                <div class="col-12 pt-3">
                    <input type="text" name="description" required   maxlength="190" class="form-control" value="{{old('description')}}" >
                </div>
            </div>

            <div class="col-12 p-3 row">

                <div class="col-12 pt-3">
                    القسم
                </div>
                <select name="category_id" class="form-control">
                    <option value=""> اختر  القسم </option>
                    @foreach($categories as $category)
                        <option value="{{ $category->id }}" {{ old('category_id') == $category->id ? 'selected' : null }}>{{ $category->title }}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-12 p-3 row">

                <div class="col-12">
                    الحاله
                </div>
                <select name="active" class="form-control">
                    <option value="1" {{ old('active') == '1' ? 'selected' : null }}>مفعل </option>
                    <option value="0" {{ old('active') == '0' ? 'selected' : null }}>غير  مفعل </option>
                </select>
            </div>




            <div class="col-12 p-2">
                <div class="col-12">
                    الصور
                </div>
                <div class="col-12 pt-3">
                    <input type="file" name="images[]" id="hotel" class="form-control" multiple="multiple">
                </div>
                <div class="col-12 pt-3">

                </div>
            </div>

		</div>

		<div class="col-12 p-3">
			<button class="btn btn-success" id="submitEvaluation">حفظ</button>
		</div>
		</form>
	</div>
</div>
@endsection

